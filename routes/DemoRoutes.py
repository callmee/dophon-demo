# coding: utf-8
import dophon
from dophon.annotation import *
from controller.controller import TestController
from message_queue import *

_DemoRou = None

app = dophon.blue_print(inject_config={
    'inj_obj_list': {
        '_DemoRou': TestController
    },
    'global_obj': globals()
},
    name='demo',
    import_name=__name__,
    template_folder='../templates')


@RequestMapping(app=app, path='/', methods=['get'])
@ResponseBody()
def test():
    print(id(app))
    return {'hahahah': '测试成功'}


@RequestMapping(app=app, path='/index', methods=['get'])
@ResponseTemplate(template=['index.html'])
def index():
    return {}


@RequestMapping(app, '/exc/500', ['get', 'post'])
def demo_exception():
    1 / 0


@RequestMapping(app, '/get/bean/1', ['get'])
def bean_1():
    b = Bean('call_beans')
    b.call()
    pass


@RequestMapping(app, '/get/bean/2', ['get'])
def bean_2():
    b = Bean('call_beans')
    b.call()
    pass


@RequestMapping(app, '/test/page/select', ['get'])
@ResponseBody()
def test_page_select():
    result = _DemoRou.test_page_select()
    return result


@RequestMapping(app, '/test/page/unselect', ['get'])
@ResponseBody()
def test_page_unselect():
    result = _DemoRou.test_page_unselect()
    return result


@RequestMapping(app, '/test/like/sql', ['get'])
@ResponseBody()
def test_like_sql():
    result = _DemoRou.test_like_sql()
    return result


@RequestMapping(app, '/test/if/sqls', ['get'])
@ResponseBody()
def test_if_sqls():
    result = _DemoRou.test_if_sqls()
    return result


@RequestMapping(app, '/test/mq/send', ['get'])
@ResponseBody()
def test_msg_queue():
    test_produce_msg()
    return {
        'msg': '发送成功'
    }
