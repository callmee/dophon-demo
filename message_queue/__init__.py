from dophon_mq import *


@producer(tag='test_tag')
def test_produce_msg():
    return 'test_msg'


class MsgConsumer(ConsumerCenter):

    @consumer(tag='test_tag', arg_name='data')
    def test_consume_msg(data):
        # print(data)
        return data

MsgConsumer()
